import Vue from 'vue'

const requireComponent = require.context(
  // Look for files in the current directory
  '.',
  //  Look in subdirectories
  true,
  /\w+(Filter).vue$/
)

requireComponent.keys().forEach((fileName) => {
  // Get the component config
  const componentConfig = requireComponent(fileName)
  // Get the PascalCase version of the component name
  const componentName = fileName
    // Remove the "./_" from the beginning
    .replace(/^\.\/_/, '')
    // Remove the file extension from the end
    .replace(/\.\w+$/, '')
    // split
    .split('/')
    // take last element
    .pop()

  // Globally register the component
  Vue.component(componentName, componentConfig.default || componentConfig)
})
