export default {
  flyTo({ commit }, coords) {
    commit('SET_COORDS', coords)
  }
}
