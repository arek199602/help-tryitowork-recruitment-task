export default {
  SET_FLATS(state, flats) {
    state.flats = flats
  },
  SET_LOADING(state, loading) {
    state.loading = loading
  },
  SET_AVERAGE_PRICE(state, averagePrice) {
    state.averagePrice = averagePrice
  }
}
