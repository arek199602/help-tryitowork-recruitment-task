import { Filter } from '@/common/flats/Filter'

export default {
  flats(state) {
    return state.flats
  },
  loading(state) {
    return state.loading
  },
  filteredByAll(state, getters, rootState, rootGetters) {
    if (rootGetters['filter/policyFiltersOn']) {
      return new Filter(state.flats)
        .getByPriceRange(rootGetters['filter/priceRange'])
        .getByBedroomQuantity(rootGetters['filter/bedroomQuantity'])
        .getByBathroomQuantity(rootGetters['filter/bathroomQuantity'])
        .getBySmokingAllowed(rootGetters['filter/smokingAllowed'])
        .getByPetsAllowed(rootGetters['filter/petsAllowed'])
        .getBySection8HousingAllowed(
          rootGetters['filter/section8HousingAllowed']
        )
        .getByPlace(rootGetters['filter/place']).filteredList
    } else {
      return new Filter(state.flats)
        .getByPriceRange(rootGetters['filter/priceRange'])
        .getByBedroomQuantity(rootGetters['filter/bedroomQuantity'])
        .getByBathroomQuantity(rootGetters['filter/bathroomQuantity'])
        .getByPlace(rootGetters['filter/place']).filteredList
    }
  }
}
