export default {
  async setFlats({ commit }) {
    commit('SET_LOADING', true)
    const { flats } = await this.$axios.$get('/flats')
    commit('SET_FLATS', flats)
    commit('SET_LOADING', false)
  },
  setAveragePrice({ commit }, averagePrice) {
    commit('SET_AVERAGE_PRICE', averagePrice)
  }
}
