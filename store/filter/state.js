export default () => ({
  priceFilter: {
    priceRange: [0, 3500]
  },
  roomFilter: {
    bedroomQuantity: 1,
    bathroomQuantity: 1
  },
  policyFilter: {
    smokingAllowed: false,
    petsAllowed: false,
    section8HousingAllowed: false
  },
  addressFilter: {
    place: 'New York'
  }
})
