export default {
  priceRange(state) {
    return state.priceFilter.priceRange
  },
  bedroomQuantity(state) {
    return state.roomFilter.bedroomQuantity
  },
  bathroomQuantity(state) {
    return state.roomFilter.bathroomQuantity
  },
  smokingAllowed(state) {
    return state.policyFilter.smokingAllowed
  },
  petsAllowed(state) {
    return state.policyFilter.petsAllowed
  },
  section8HousingAllowed(state) {
    return state.policyFilter.section8HousingAllowed
  },
  place(state) {
    return state.addressFilter.place
  },
  policyFiltersOn(state) {
    return !!Object.values(state.policyFilter).filter((filter) => filter).length
  }
}
