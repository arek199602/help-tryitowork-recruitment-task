export default {
  setPriceRange({ commit }, priceRange) {
    commit('SET_PRICE_RANGE', priceRange)
  },
  setBedroomQuantity({ commit }, bedroomQuantity) {
    commit('SET_BEDROOM_QUANTITY', bedroomQuantity)
  },
  setBathroomQuantity({ commit }, bathroomQuantity) {
    commit('SET_BATHROOM_QUANTITY', bathroomQuantity)
  },
  setSmokingPolicy({ commit }, smokingAllowed) {
    commit('SET_SMOKING_POLICY', smokingAllowed)
  },
  setPetsPolicy({ commit }, petsAllowed) {
    commit('SET_PETS_POLICY', petsAllowed)
  },
  setSection8HousingPolicy({ commit }, section8HousingAllowed) {
    commit('SET_SECTION_8_HOUSING_POLICY', section8HousingAllowed)
  },
  setPlace({ commit }, place) {
    commit('SET_PLACE', place)
  }
}
