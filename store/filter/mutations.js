export default {
  SET_PRICE_RANGE(state, priceRange) {
    state.priceFilter.priceRange = priceRange
  },
  SET_BEDROOM_QUANTITY(state, bedroomQuantity) {
    state.roomFilter.bedroomQuantity = bedroomQuantity
  },
  SET_BATHROOM_QUANTITY(state, bathroomQuantity) {
    state.roomFilter.bathroomQuantity = bathroomQuantity
  },
  SET_SMOKING_POLICY(state, smokingAllowed) {
    state.policyFilter.smokingAllowed = smokingAllowed
  },
  SET_PETS_POLICY(state, petsAllowed) {
    state.policyFilter.petsAllowed = petsAllowed
  },
  SET_SECTION_8_HOUSING_POLICY(state, section8HousingAllowed) {
    state.policyFilter.section8HousingAllowed = section8HousingAllowed
  },
  SET_PLACE(state, place) {
    state.addressFilter.place = place
  }
}
