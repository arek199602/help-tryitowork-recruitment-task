class Filter {
  constructor(flats) {
    this.filteredList = flats
  }

  getByPriceRange = (priceRange) => {
    if (!priceRange.length) return this.filteredList
    this.filteredList = this.filteredList.filter((flat) => {
      return flat.price > priceRange[0] && flat.price < priceRange[1]
    })
    return this
  }

  getByBedroomQuantity = (bedroomQuantity) => {
    this.filteredList = this.filteredList.filter((flat) => {
      return (
        flat.rooms.find((room) => {
          return room.type === 'bedroom'
        }).quantity >= bedroomQuantity
      )
    })
    return this
  }

  getByBathroomQuantity = (bathroomQuantity) => {
    this.filteredList = this.filteredList.filter((flat) => {
      return (
        flat.rooms.find((room) => {
          return room.type === 'bathroom'
        }).quantity >= bathroomQuantity
      )
    })
    return this
  }

  getBySmokingAllowed = (smokingAllowed) => {
    this.filteredList = this.filteredList.filter((flat) => {
      return (
        flat.policies.find((policy) => {
          return policy.type === 'smoking'
        }).allowed === smokingAllowed
      )
    })
    return this
  }

  getByPetsAllowed = (petsAllowed) => {
    this.filteredList = this.filteredList.filter((flat) => {
      return (
        flat.policies.find((policy) => {
          return policy.type === 'pets'
        }).allowed === petsAllowed
      )
    })
    return this
  }

  getBySection8HousingAllowed = (section8HousingAllowed) => {
    this.filteredList = this.filteredList.filter((flat) => {
      return (
        flat.policies.find((policy) => {
          return policy.type === 'section8Housing'
        }).allowed === section8HousingAllowed
      )
    })
    return this
  }

  getByPlace = (placeName) => {
    this.filteredList = this.filteredList.filter((flat) => {
      return flat.address.place_name.match(placeName)
    })
    return this
  }
}

export { Filter }
