const deepClone = (object) => {
  return JSON.parse(JSON.stringify(object))
}

const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

export { deepClone, numberWithCommas }
